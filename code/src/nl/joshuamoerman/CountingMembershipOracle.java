package nl.joshuamoerman;

import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;

import java.io.PrintStream;
import java.util.Collection;

/**
 * An membership oracle which maintains a count of the number of queries (and symbols). Useful for testing
 * performance of equivalence oracles (which use membership oracles). It simply delegates the queries to
 * a delegate.
 * @param <I> Input alphabet
 * @param <D> Output domain (should be Word<O> for mealy machines)
 */
public class CountingMembershipOracle<I, D> implements MembershipOracle<I, D> {
    private final MembershipOracle<I, D> delegate;
    private final String name;
    private CountPair cp = new CountPair();

    public static class CountPair {
        public long queries = 0;
        public long symbols = 0;

        public CountPair(long queries, long symbols) {
            this.queries = queries;
            this.symbols = symbols;
        }

        public CountPair(CountPair cp) {
            this.queries = cp.queries;
            this.symbols = cp.symbols;
        }

        public CountPair() {}

        public CountPair reset() {
            CountPair copy = new CountPair(this);
            queries = 0;
            symbols = 0;
            return copy;
        }
    }

    public CountingMembershipOracle(MembershipOracle<I, D> delegate, String name){
        this.delegate = delegate;
        this.name = name;
    }

    public CountPair reset(){
        return cp.reset();
    }

    public void log(PrintStream output, boolean newline){
        if(newline)
            output.println(name + " " + cp.queries + " " + cp.symbols);
        else
            output.print(name + " " + cp.queries + " " + cp.symbols);
    }

    public CountPair logAndReset(PrintStream output, boolean newline){
        log(output, newline);
        return reset();
    }

    @Override
    public void processQueries(Collection<? extends Query<I, D>> collection) {
        cp.queries += collection.size();
        // we can't do a parallelStream here, because the writes to x give a data race
        collection.forEach((Query<I, D> idQuery) -> cp.symbols += idQuery.getInput().size());

        delegate.processQueries(collection);
    }
}
