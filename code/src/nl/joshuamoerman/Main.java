package nl.joshuamoerman;

import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.SimulatorOracle;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;
import nl.joshuamoerman.NBitRegisterMachine.Symbols;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static nl.joshuamoerman.NBitRegisterMachine.NBitRegisterMachineOracle;
import static nl.joshuamoerman.NBitRegisterMachine.Symbols.*;

public class Main {
    // Generic simple learning setup
    static <I, O> void learnIt(boolean useProductLearner, int dimensionality, Alphabet<I> alphabet, MembershipOracle<I, Word<List<O>>> membershipOracle) {
        LocalDateTime start = LocalDateTime.now();
        System.out.println("LOG> start: " + start);

        // We will count both the number of queries for testing and for learning. This is important, since
        // they might influence each other (shorter testing might lead to more learning, and vice versa).
        // In the end we are really interested in the combined result (but this is part of the data
        // post processing).
        CountingMembershipOracle<I, Word<List<O>>> mOracleForLearning = new CountingMembershipOracle<>(membershipOracle, "learning");
        CountingMembershipOracle<I, Word<List<O>>> mOracleForTesting = new CountingMembershipOracle<>(membershipOracle, "testing");

        // We will use a conformance tester for the implementing the equivalence queries.
        // We could do a bisimulation, but this is closer to how learning is applied on software.
        EquivalenceOracle<MealyMachine<?, I, ?, List<O>>, I, Word<List<O>>> eqOracle = new RandomizedEQOracle<>(mOracleForTesting, 1, 10);

        // Learning algorithm, either a direct TTT algorithm, or using the product learner
        LearningAlgorithm<MealyMachine<?, I, ?, List<O>>, I, Word<List<O>>> learner;
        if (useProductLearner) {
            learner = new ProductLearner<>(dimensionality, alphabet, mOracleForLearning, AcexAnalyzers.LINEAR_FWD);
        } else {
            learner = new TTTLearnerMealy<>(alphabet, mOracleForLearning, AcexAnalyzers.LINEAR_FWD);
        }

        // Here we will perform our experiment. We write the loop manually in order to do some logging
        int stage = 0;
        learner.startLearning();

        while (true) {
            stage++;
            MealyMachine<?, I, ?, List<O>> hypothesis = learner.getHypothesisModel();

            // Print statistics
            LocalDateTime now = LocalDateTime.now();
            System.out.println(stage + ": " + now + " duration so far: " + Duration.between(start, now));
            if(useProductLearner) {
                StringBuilder sb = new StringBuilder();
                ProductLearner<I, O>.ProductMachine<Object, Object> ph = (ProductLearner.ProductMachine)hypothesis;
                for (MealyMachine h : ph.getComponents()) {
                    sb.append(h.size());
                    sb.append(' ');
                }
                System.out.println("States: " + sb.toString());
            } else {
                System.out.println("States: " + hypothesis.size());
            }
            mOracleForLearning.log(System.out, true);
            mOracleForTesting.log(System.out, true);
            System.out.println();

            // Search for CE
            DefaultQuery<I, Word<List<O>>> ce = eqOracle.findCounterExample(hypothesis, alphabet);
            if (ce == null) break;

            // Rinse and repeat
            learner.refineHypothesis(ce);
        }

        System.out.println("LOG> Done with learning");
        System.exit(0);
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.err.println("Usage: ./Main register <n> <useProdLearner> OR ./Main circuit <file> <useProdLearner>");
            System.exit(1);
        }

        if (args[0].equals("register")) {
            // User can provide size of bit register machine
            int dimensionality = Integer.parseInt(args[1]);
            boolean useProductLearner = Boolean.parseBoolean(args[2]);

            System.out.println("LOG> dim: " + dimensionality);

            // The alphabet consists of all actions
            Alphabet<Symbols> alphabet = Alphabets.fromArray(L, R, F);

            // The membership oracle is implemented in the NBitRegisterMachine class.
            // implemented as a Mealy machine, which outputs the current state after each
            // transition. This is almost the same as the corresponding Moore machine, except
            // that the first state won't give output.
            MembershipOracle<Symbols, Word<List<Boolean>>> membershipOracle = new NBitRegisterMachineOracle(dimensionality, true);

            // Ready, set, go!
            learnIt(useProductLearner, dimensionality, alphabet, membershipOracle);

        } else if (args[0].equals("circuit")) {
            // User can provide a filename (to a dot file representing a circuit)
            String file = args[1];
            boolean useProductLearner = Boolean.parseBoolean(args[2]);

            // Read the file and get machine
            CircuitParser parser = new CircuitParser(Paths.get(file));
            CompactMealy<String, List<String>> fm = parser.createMachine();
            int dimensionality = parser.dimensionality;

            System.out.println("LOG> dim: " + dimensionality);

            // Create simulator to implement oracles
            MembershipOracle<String, Word<List<String>>> membershipOracle = new SimulatorOracle.MealySimulatorOracle<>(fm);

            // Ready, set, go!
            learnIt(useProductLearner, dimensionality, fm.getInputAlphabet(), membershipOracle);

        } else {
            System.err.println("Usage: ./Main register <n> OR ./Main circuit <file>");
            System.exit(1);
        }
    }
}
