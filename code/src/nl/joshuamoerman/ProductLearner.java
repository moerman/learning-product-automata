package nl.joshuamoerman;

import com.google.common.collect.Lists;
import de.learnlib.acex.AcexAnalyzer;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * This class implements the product learner by using a separate learner for each
 * component. The dimensionality has to be provided by the users (although it could
 * be determined by the first-read output, theoretically). All lists are of the
 * same size.
 */
public class ProductLearner<I, O> implements LearningAlgorithm.MealyLearner<I, List<O>> {
    final private List<MealyLearner<I, O>> learners;
    final private Alphabet<I> alphabet;

    public ProductLearner(int dimensionality, Alphabet<I> alphabet, MembershipOracle<I, Word<List<O>>> oracle, AcexAnalyzer analyzer) {
        this.alphabet = alphabet;
        learners = Lists.newArrayListWithCapacity(dimensionality);
        for (int i = 0; i < dimensionality; ++i) {
            learners.add(new TTTLearnerMealy<>(alphabet, new IndexedOracle(oracle, i), analyzer));
        }
    }

    @Override
    public void startLearning() {
        for (MealyLearner l : learners) {
            l.startLearning();
        }
    }

    @Override
    public boolean refineHypothesis(@Nonnull DefaultQuery<I, Word<List<O>>> ce) {
        boolean refined = false;
        for (int i = 0; i < learners.size(); i++) {
            // LearnLib algorithms ignore the counter example if it's not a counter example
            boolean r = learners.get(i).refineHypothesis(getComponent(i, ce));
            refined |= r;
        }
        return refined;
    }

    @Nonnull
    @Override
    public MealyMachine<?, I, ?, List<O>> getHypothesisModel() {
        List<MealyMachine<Object, I, Object, O>> hypos = Lists.newArrayListWithCapacity(learners.size());
        for (int i = 0; i < learners.size(); i++) {
            // This cast is fine. However, the whole class could be generalized
            // to get rid of the cast, but then we'll have to pass the types S and T
            // around everywhere, something I wanted to avoid.
            hypos.add((MealyMachine<Object, I, Object, O>) learners.get(i).getHypothesisModel());
        }
        return new ProductMachine<>(hypos, alphabet);
    }

    DefaultQuery<I, Word<O>> getComponent(int index, DefaultQuery<I, Word<List<O>>> query) {
        Word<List<O>> output = query.getOutput();
        WordBuilder<O> wb = new WordBuilder<>(output.size());
        for (List<O> s : output) {
            wb = wb.append(s.get(index));
        }
        return new DefaultQuery<>(query.getPrefix(), query.getSuffix(), wb.toWord());
    }

    // Each learner will have to talk to an oracle which returns single components
    // as output. We need to "wrap" the main oracle, the wrapper will then extract
    // the right component.
    class IndexedOracle implements MembershipOracle<I, Word<O>> {
        final private MembershipOracle<I, Word<List<O>>> delegate;
        final private int index;

        IndexedOracle(MembershipOracle<I, Word<List<O>>> delegate, int index) {
            this.delegate = delegate;
            this.index = index;
        }

        @Override
        public void processQueries(Collection<? extends Query<I, Word<O>>> queries) {
            Collection<WrappedQuery> wrappedQueries = Lists.newArrayListWithCapacity(queries.size());
            for (Query<I, Word<O>> q : queries) {
                wrappedQueries.add(new WrappedQuery(q));
            }
            delegate.processQueries(wrappedQueries);
        }

        // The corresponding wrapper for queries
        class WrappedQuery extends Query<I, Word<List<O>>> {
            final private Query<I, Word<O>> original;

            WrappedQuery(Query<I, Word<O>> original) {
                this.original = original;
            }

            @Nonnull
            @Override
            public Word<I> getPrefix() {
                return original.getPrefix();
            }

            @Nonnull
            @Override
            public Word<I> getSuffix() {
                return original.getSuffix();
            }

            @Override
            public void answer(@Nullable Word<List<O>> output) {
                WordBuilder<O> wb = new WordBuilder<>(output.size());
                for (List<O> symbol : output) {
                    wb = wb.append(symbol.get(index));
                }
                original.answer(wb.toWord());
            }
        }
    }

    // The hypothesis model is a product automaton
    public class ProductMachine<S, T> implements MealyMachine<List<S>, I, List<T>, List<O>> {
        final List<MealyMachine<S, I, T, O>> components;
        final Alphabet<I> alphabet;
        Collection<List<S>> cachedStates;

        ProductMachine(List<MealyMachine<S, I, T, O>> components, Alphabet<I> alphabet) {
            this.components = components;
            this.alphabet = alphabet;
        }

        public List<MealyMachine<S, I, T, O>> getComponents() {
            return components;
        }

        @Nullable
        @Override
        public List<O> getTransitionOutput(List<T> ts) {
            List<O> out = Lists.newArrayListWithCapacity(components.size());
            for (int i = 0; i < components.size(); i++) {
                out.add(components.get(i).getTransitionOutput(ts.get(i)));
            }
            return out;
        }

        @Nonnull
        @Override
        public Collection<List<S>> getStates() {
            if(cachedStates != null) {
                return cachedStates;
            }
            // We can either return ALL states
            // or just the reachable ones
            boolean trimAutomaton = true;
            if (trimAutomaton) {
                // We do a BFS
                Set<List<S>> reach = new HashSet<>();
                Queue<List<S>> bfsQueue = new ArrayDeque<>();

                List<S> init = getInitialState();

                bfsQueue.add(init);

                List<S> curr;
                while ((curr = bfsQueue.poll()) != null) {
                    // we can skip it, if we already handled it
                    if(reach.contains(curr)) continue;

                    for (I in : alphabet) {
                        List<S> succ = getSuccessor(curr, in);
                        if (succ == null) continue;

                        if (!reach.contains(succ)) {
                            bfsQueue.add(succ);
                        }
                    }

                    reach.add(curr);
                }

                cachedStates = reach;
            } else {
                // We will enumerate all the states in the product
                // we start with a single state: the empty vector
                Collection<List<S>> allStates = Lists.newArrayList();
                allStates.add(Lists.newArrayList());

                // then we append the states from each component
                for (MealyMachine<S, I, T, O> component : components) {
                    Collection<List<S>> newAllStates = Lists.newArrayList();

                    // for each state in current component
                    for (S s : component.getStates()) {
                        // for each state given by previous components
                        for (List<S> stateList : allStates) {
                            // copy and append
                            List<S> newState = Lists.newArrayList(stateList);
                            newState.add(s);
                            newAllStates.add(newState);
                        }
                    }

                    // for the next batch
                    allStates = newAllStates;
                }

                cachedStates = allStates;
            }
            return cachedStates;
        }

        @Nullable
        @Override
        public List<T> getTransition(List<S> s, @Nullable I a) {
            List<T> out = Lists.newArrayListWithCapacity(components.size());
            for (int i = 0; i < components.size(); i++) {
                out.add(components.get(i).getTransition(s.get(i), a));
            }
            return out;
        }

        @Nonnull
        @Override
        public List<S> getSuccessor(List<T> ts) {
            List<S> out = Lists.newArrayListWithCapacity(components.size());
            for (int i = 0; i < components.size(); i++) {
                out.add(components.get(i).getSuccessor(ts.get(i)));
            }
            return out;
        }

        @Nullable
        @Override
        public List<S> getInitialState() {
            List<S> out = Lists.newArrayListWithCapacity(components.size());
            for (int i = 0; i < components.size(); i++) {
                out.add(components.get(i).getInitialState());
            }
            return out;
        }
    }
}
