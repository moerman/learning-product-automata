package nl.joshuamoerman;

import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import org.testng.collections.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This has nothing to do with registers. It is an example of an automaton in the paper
 * by Rivest and Schapire on learning diversities. The states space has size n2^n, but
 * the number of diversities (= number of states in reverse automaton) is just 2n.
 * Note that the non-wrapping version has bigger diversity.
 *
 * We use it as an example of how products can be exploited during L* learning.
 * The output will be a bit-vector. The length of this vector is constant (the Java
 * type system has no way to enforce this, I think).
 *
 * We implement this as a Mealy machine, which outputs the current state after each
 * transition. This is almost the same as the corresponding Moore machine, except
 * that the first state won't give output.
 */
public class NBitRegisterMachine {
    enum Symbols { L, R, F }

    // this is the state: a bitvector and a position
    private List<Boolean> bits;
    private int index;

    // this controls the behaviour
    private boolean wrapAround;

    // You can choose the size, and its behaviour on the edges
    public NBitRegisterMachine(int size, boolean wrap) {
        // starts with 0-bit vector of length n
        bits = new ArrayList<>(Collections.nCopies(size, false));
        // start at initial position
        index = 0;
        wrapAround = wrap;
    }

    public List<Boolean> act(Symbols s) {
        switch (s) {
            case L: left(); break;
            case R: right(); break;
            case F: flip(); break;
        }

        return Lists.newArrayList(bits);
    }

    public void reset() {
        int size = bits.size();
        bits.clear();
        bits.addAll(Collections.nCopies(size, false));
        index = 0;
    }

    private void left() {
        index--;
        if(index < 0)
            index = wrapAround ? bits.size() - 1 : 0;
    }

    private void right() {
        index++;
        if(index >= bits.size())
            index = wrapAround ? 0 : bits.size() - 1;
    }

    private void flip() {
        bits.set(index, !bits.get(index));
    }

    /* We provide a LearnLib oracle interface for this machine.
     * It simply runs the machine and collects output.
     */
    public static class NBitRegisterMachineOracle implements MembershipOracle<Symbols, Word<List<Boolean>>> {
        final private NBitRegisterMachine instance;

        public NBitRegisterMachineOracle(int size, boolean wrap) {
            instance = new NBitRegisterMachine(size, wrap);
        }

        @Override
        public void processQueries(Collection<? extends Query<Symbols, Word<List<Boolean>>>> collection) {
            for (Query<Symbols, Word<List<Boolean>>> q : collection) {
                WordBuilder<List<Boolean>> wb = new WordBuilder<>(q.getSuffix().size());

                instance.reset();
                for(Symbols s : q.getPrefix()) {
                    instance.act(s);
                }
                for(Symbols s : q.getSuffix()) {
                    wb = wb.append(instance.act(s));
                }
                q.answer(wb.toWord());
            }
        }
    }
}
