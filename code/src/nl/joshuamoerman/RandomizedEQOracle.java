package nl.joshuamoerman;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.UniversalDeterministicAutomaton;
import net.automatalib.automata.concepts.Output;
import net.automatalib.util.automata.Automata;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;

/**
 * This testing method is based on the randomized Wp method in LearnLib.
 * I removed the suffix part, as it took too long to compute the state identifiers
 * for some of the bigger product automata. The prefix + random part has also
 * been used in some successful learning applications.
 */
public class RandomizedEQOracle<A extends UniversalDeterministicAutomaton<?, I, ?, ?, ?> & Output<I, D>, I, D>
        implements EquivalenceOracle<A, I, D> {
    private final MembershipOracle<I, D> sulOracle;
    private final int minimalSize;
    private final int rndLength;
    private final int bound;

    /**
     * Constructor for an unbounded testing oracle
     *
     * @param sulOracle   oracle which answers tests.
     * @param minimalSize minimal size of the random word
     * @param rndLength   expected length (in addition to minimalSize) of random word
     */
    public RandomizedEQOracle(MembershipOracle<I, D> sulOracle, int minimalSize, int rndLength) {
        this.sulOracle = sulOracle;
        this.minimalSize = minimalSize;
        this.rndLength = rndLength;
        this.bound = 0;
    }

    /**
     * Constructor for a bounded testing oracle
     *
     * @param sulOracle   oracle which answers tests.
     * @param minimalSize minimal size of the random word
     * @param rndLength   expected length (in addition to minimalSize) of random word
     * @param bound       specifies the bound (set to 0 for unbounded).
     */
    public RandomizedEQOracle(MembershipOracle<I, D> sulOracle, int minimalSize, int rndLength, int bound) {
        this.sulOracle = sulOracle;
        this.minimalSize = minimalSize;
        this.rndLength = rndLength;
        this.bound = bound;
    }

    @Override
    @ParametersAreNonnullByDefault
    public DefaultQuery<I, D> findCounterExample(A hypothesis, Collection<? extends I> inputs) {
        UniversalDeterministicAutomaton<?, I, ?, ?, ?> aut = hypothesis;
        Output<I, D> out = hypothesis;
        return doFindCounterExample(aut, out, inputs);
    }

    private <S> DefaultQuery<I, D> doFindCounterExample(UniversalDeterministicAutomaton<S, I, ?, ?, ?> hypothesis,
                                                        Output<I, D> output, Collection<? extends I> inputs) {
        // Note that we want to use ArrayLists because we want constant time random access
        // We will sample from this for a prefix
        ArrayList<Word<I>> stateCover = new ArrayList<>(hypothesis.size());
        Automata.cover(hypothesis, inputs, stateCover, null);

        // Then repeatedly from this for a random word
        ArrayList<I> arrayAlphabet = new ArrayList<>(inputs);

        Random rand = new Random();
        int currentBound = bound;
        while (bound == 0 || currentBound-- > 0) {
            WordBuilder<I> wb = new WordBuilder<>(minimalSize + rndLength + 1);

            // pick a random state
            wb.append(stateCover.get(rand.nextInt(stateCover.size())));

            // construct random middle part (of some expected length)
            int size = minimalSize;
            while ((size > 0) || (rand.nextDouble() > 1 / (rndLength + 1.0))) {
                wb = wb.append(arrayAlphabet.get(rand.nextInt(arrayAlphabet.size())));
                if (size > 0) size--;
            }

            Word<I> queryWord = wb.toWord();
            DefaultQuery<I, D> query = new DefaultQuery<>(queryWord);
            D hypOutput = output.computeOutput(queryWord);
            sulOracle.processQueries(Collections.singleton(query));
            if (!Objects.equals(hypOutput, query.getOutput()))
                return query;
        }

        // no counter example found within the bound
        return null;
    }
}

