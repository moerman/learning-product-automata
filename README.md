Learning product automata
=========================

In this short paper we discuss how one can use decomposition of product
automata to improve the L* learning algorithm. The repository contains the
source code (it is implemented in Java with the LearnLib framework) and
models. The models can also be found on http://automata.cs.ru.nl/.
