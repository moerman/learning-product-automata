\documentclass[pmlr]{jmlr}
% NOTE to editor: I have added a pagebreak at line 268 in order to prevent a floating
% figure (Algorithm 1) to break the list environment of Lemma 5.
\usepackage{algorithm}
\usepackage[noend]{algorithmic}
\usepackage{array}

% For diagrams and plots
\usepackage{tikz}
\usetikzlibrary{chains,shapes,fit}
\usepackage{pgfplots}

% Used for more natural spacing in Lemma 5
\usepackage{enumitem}

% for brackets [| ... |]
\usepackage{stmaryrd}

% for defining terminology
\newcommand{\Def}[1]{\emph{{#1}}}

% Notation
\newcommand{\lsem}{\llbracket}
\newcommand{\rsem}{\rrbracket}
\newcommand{\BB}{\mathbb{B}}
\newcommand{\LStar}{\texttt{L\textsuperscript{*}}}
\newcommand{\TTT}{\texttt{TTT}}
\newcommand{\bigO}{\mathcal{O}}


\title{Learning Product Automata}

\author{\Name{Joshua Moerman} \Email{joshua.moerman@cs.ru.nl} \\
	\addr Institute for Computing and Information Sciences\\
	Radboud University\\
	Nijmegen, the Netherlands}

\editor{Editor's name}


\begin{document}

\maketitle

\begin{abstract}
We give an optimisation for active learning algorithms,
applicable to learning Moore machines with decomposable outputs.
These machines can be decomposed themselves by projecting on each output. This results in smaller components that can then be learnt with fewer queries.
We give experimental evidence that this is a useful technique which can reduce the number of queries substantially.
Only in some cases the performance is worsened by the slight overhead.
Compositional methods are widely used throughout engineering, and the decomposition presented in this article promises to be particularly interesting for learning hardware systems.
\end{abstract}

\begin{keywords}
	query learning, product automata, composition
\end{keywords}


\section{Introduction}
Query learning (or, active learning) is becoming a valuable tool in engineering of both hardware and software systems \citep{DBLP:journals/cacm/Vaandrager17}.
Indeed, applications can be found in a broad range of applications:
finding bugs in network protocols as shown by \citet{DBLP:conf/cav/Fiterau-Brostean16,DBLP:conf/spin/Fiterau-Brostean17},
assisting with refactoring legacy software as shown by \citet{DBLP:conf/ifm/SchutsHV16}, and
reverse engineering bank cards by \citet{DBLP:conf/woot/ChaluparPPR14}.

These learning techniques originate from the field of grammatical inference.
One of the crucial steps for applying these to black box systems was to move from deterministic finite automata to deterministic Moore or Mealy machines, capturing reactive systems with any kind of output.
With little adaptations, the algorithms work well, as shown by the many applications.
This is remarkable, since little specific knowledge is used beside the input alphabet of actions.

Realising that composition techniques are ubiquitous in engineering, we aim to use more structure of the system during learning.
In the present paper we use the simplest type of composition; we learn product automata, where outputs of several components are simply paired.
Other types of compositions, such as sequential composition of Mealy machines, are discussed in Section~\ref{sec:discussion}.

To the best of the author's knowledge, this has not been done before explicitly.
Furthermore, libraries such as LearnLib \citep[see][]{DBLP:conf/cav/IsbernerHS15} and libalf \citep[see][]{DBLP:conf/cav/BolligKKLNP10} do not include such functionality out of the box.
\emph{Implicitly}, however, it has been done before.
\citet{DBLP:journals/jacm/RivestS94} use two tricks to reduce the size of some automata in their paper ``Diversity-based inference of finite automata''.
The first trick is to look at the \emph{reversed automaton} (in their terminology, the \emph{diversity-based automaton}).
The second trick (which is not explicitly mentioned, unfortunately) is to have a different automaton for each observable (i.e., output).
In one of their examples the two tricks combined give a reduction from $\pm 10^{19}$ states to just $54$ states.

In this paper, we isolate this trick and use it in query learning.
We give an extension of \LStar{} which handles products directly and we give a second algorithm which simply runs two learners simultaneously.
Furthermore, we argue that this is particularly interesting in the context of model learning of hardware, as systems are commonly engineered in a compositional way.
We give preliminary experimental evidence that the technique works and improves the learning process.
As benchmarks, we learn (simulated) circuits which provide several output bits.


\section{Preliminaries}

We use the formalism of Moore machines to describe our algorithms.
Nonetheless, the results can also be phrased in terms of Mealy machines.

\begin{definition}
	A \Def{Moore machine} is a tuple $M = (Q, I, O, \delta, o, q_0)$
	where $Q, I$ and $O$ are finite sets of states, inputs and outputs respectively,
	$\delta \colon Q \times I \to Q$ is the transition function,
	$o \colon Q \to O$ is the output function,
	and $q_0 \in Q$ is the initial state.
	We define the size of the machine, $|M|$, to be the cardinality of $Q$.
\end{definition}

We extend the definition of the transition function to words as $\delta \colon Q \times I^\ast \to Q$.
The \Def{behaviour} of a state $q$ is the map $\lsem q \rsem \colon I^\ast \to O$ defined by $\lsem q \rsem(w) = o(\delta(q, w))$.
Two states $q,q'$ are \Def{equivalent} if $\lsem q \rsem = \lsem q' \rsem$.
A machine is \Def{minimal} if all states have different behaviour and all states are reachable.
We will often write $\lsem M \rsem$ to mean $\lsem q_0 \rsem$ and say that machines are equivalent if their initial states are equivalent.

\begin{definition}
	Given two Moore machines with equal input sets, $M_1 = (Q_1, I, O_1, \delta_1, o_1, {q_0}_1)$ and $M_2 = (Q_2, I, O_2, \delta_2, o_2, {q_0}_2)$,
	we define their \Def{product} $M_1 \times M_2$ by:
	\[ M_1 \times M_2 = (Q_1 \times Q_2, I, O_1 \times O_2, \delta, o, ({q_0}_1, {q_0}_2)), \]
	where $ \delta((q_1, q_2), a) = (\delta_1(q_1, a), \delta_2(q_2, a)) $ and
	$ o((q_1, q_2)) = (o_1(q_1), o_2(q_2)) $.
\end{definition}

\begin{figure}
	\centering
	\begin{tikzpicture}[minimum size=3em, node distance=5.5em,shorten >=1pt]
	\node[draw] (A) {$M$};
	
	\path[->]
	(A.west) ++(-3em, 0) edge node [above left=-10pt] {$i$} (A.west)
	(A.east) ++(0, -1em) edge node [above right=-10pt] {$o_1$} ++(3em, 0)
	(A.east) ++(0, 1em) edge node [above right=-10pt] {$o_2$} ++(3em, 0);
	
	\node (L2) [right=5cm of A] {};
	\node[draw,minimum size=2.2em] (M1) [above=-1em of L2] {$M_1$};
	\node[draw,minimum size=2.2em] (M2) [below=-1em of L2] {$M_2$};
	\node (R2) [right of=M1] {};
	
	\path (L2) ++(-5.5em, 0) edge node [above left=-10pt] {$i$} ++(2.6em, 0);
	\path[->]
	(L2) ++(-3em, 0) edge (M1.west)
	(L2) ++(-3em, 0) edge (M2.west)
	(M1.east) edge node [above right=-10pt] {$o_1$} ++(3em, 0)
	(M2.east) edge node [above right=-10pt] {$o_2$} ++(3em, 0);
	\end{tikzpicture}
	\caption{A Moore machine with two outputs (left) can be equivalently seen as two (potentially smaller) Moore machines with a single output each (right).}
	\label{fig:parallel-composition}
\end{figure}

The product is formed by running both machines in parallel and letting $I$ act on both machine synchronously.
The output of both machines is observed.
Note that the product Moore machine might have unreachable states, even if the components are reachable.
The product of more than two machines is defined by induction.

Let $M$ be a machine with outputs in $O_1 \times O_2$.
By post-composing the output function with projection functions we get two machines, called \Def{components}, $M_1$ and $M_2$ with outputs in $O_1$ and $O_2$ respectively.
This is depicted in Figure~\ref{fig:parallel-composition}.
Note that $M$ is equivalent to ${M_1 \times M_2}$.
If $M$ and its components $M_i$ are taken to be minimal, then we have ${|M| \leq |M_1| \cdot |M_2|}$ and ${|M_i| \leq |M|}$.
In the best case we have $|M_i| = \sqrt{|M|}$ and so the behaviour of $M$ can be described using only $2\sqrt{|M|}$ states, which is less than $|M|$ (if $|M| > 4$).
With iterated products the reduction can be more substantial as shown in the following example.
This reduction in state space is beneficial for learning algorithms.

We introduce basic notation: $\pi_i \colon A_1 \times A_2 \to A_i$ are the usual projection functions.
On a function $f \colon X \to A_1 \times A_2$ we use the shorthand $\pi_i f$ to denote $\pi_i \circ f$.
As usual, $uv$ denotes concatenation of string $u$ and $v$, and this is lifted to sets of strings $UV = \{uv \mid u \in U, v \in V\}$.
We define the set $[n] = \{1, \dots, n\}$ and the set of Boolean values $\BB = \{0, 1\}$.


\subsection{Example}
We take the \Def{$n$-bit register machine} example from \cite{DBLP:journals/jacm/RivestS94}.
The state space of the $n$-bit register machine $M_n$ is given by $n$ bits and a position of the reading/writing head, see Figure~\ref{fig:bitmachine}.
The inputs are commands to control the position of the head and to flip the current bit. The output is the current bit vector.
Formally it is defined as $M_n = (\BB^n \times [n], \{L, R, F\}, \BB^n, \delta, o, i)$, where the initial state is $i = ((0, \dots, 0), 1)$ and the output is $o(((b_1, \dots, b_n), k)) = (b_1, \dots, b_n)$.
The transition function is defined such that
$L$ moves the head to the left, $R$ moves the head to the right (and wraps around on either ends), and $F$ flips the current bit.
Formally,
\begin{eqnarray*}
	\delta(((b_1, \dots, b_n), k), L) &=& \begin{cases}
	((b_1, \dots, b_n), k-1) \quad &\text{if } k > 1, \\
	((b_1, \dots, b_n), n) \quad &\text{if } k = 1,
	\end{cases} \\
	\delta(((b_1, \dots, b_n), k), R) &=& \begin{cases}
	((b_1, \dots, b_n), k+1) \quad &\text{if } k < n, \\
	((b_1, \dots, b_n), 1) \quad &\text{if } k = n,
	\end{cases} \\
	\delta(((b_1, \dots, b_n), k), F) &=& ((b_1, \dots, \neg b_k, \dots, b_n), k).
\end{eqnarray*}

\begin{figure}
	\centering
	\begin{tikzpicture}
	\tikzstyle{bitcell}=[draw,minimum size=0.6cm]
	\tikzstyle{machinehead}=[arrow box,draw,minimum size=.37cm,arrow box arrows={north:.25cm}]
	
	%% Draw TM tape
	\begin{scope}[start chain=1 going right,node distance=-0.15mm]
	\node [on chain=1,bitcell] {0};
	\node [on chain=1,bitcell] {0};
	\node [on chain=1,bitcell] {1};
	\node [on chain=1,bitcell] (input) {0};
	\node [on chain=1,bitcell] {0};
	\node [on chain=1,bitcell] {1};
	\node [on chain=1,bitcell] {0};
	\node [on chain=1,bitcell] {1};
	\end{scope}
	
	%% Draw TM head below (input) tape cell
	\node [machinehead,yshift=-.3cm] at (input.south) (head) {};
	
	\end{tikzpicture}
	\caption{A state of the $8$-bit register machine.}
	\label{fig:bitmachine}
\end{figure}

The machine $M_n$ is minimal and has $n \cdot 2^n$ states.
So although this machine has very simple behaviour, learning it will require a lot of queries because of its size.
Luckily, the machine can be decomposed into smaller components.
For each bit $l$, we define a component $M_n^l = (\BB \times [n], \{L,R,F\}, \BB, \delta^l, \pi_1, (0,1))$ which only stores one bit and the head position.
The transition function $\delta^l$ is defined similarly as before on $L$ and $R$, but only flips the bit on $L$ if the head is on position $l$ (i.e., $\delta^l((b, l), F) = (\neg b, l)$ and $\delta^l((b, k)) = (b, k)$ if $k \neq l$).

The product $M_n^1 \times \dots \times M_n^n$ is equivalent to $M_n$.
Each of the components $M_n^l$ is minimal and has only $2n$ states.
So by this decomposition, we only need $2 \cdot n^2$ states to describe the whole behaviour of $M_n$.
Note, however, that the product $M_n^1 \times \dots \times M_n^n$ is not minimal; many states are unreachable.


\section{Learning}

We describe two approaches for active learning of product machines.
One is a direct extension of the well-known \LStar{} algorithm.
The other reduces the problem to any active learning algorithm, so that one can use more optimised algorithms.

We fix an unknown target machine $M$ with a known input alphabet $I$ and output alphabet $O = O_1 \times O_2$.
The goal of the learning algorithm is to infer a machine equivalent to $M$, given access to a \Def{minimally adequate teacher} as introduced by \citet{DBLP:journals/iandc/Angluin87}.
The teacher will answer the following two types of queries.
\begin{itemize}
	\item \Def{Membership queries} (MQs):
	The query consists of a word $w \in I^\ast$ and the teacher will answer with the output $\lsem M \rsem (w) \in O$.
	\item \Def{Equivalence queries} (EQs):
	The query consists of a Moore machine $H$, the \emph{hypothesis}, and the teacher will answer with \texttt{YES} if $M$ and $H$ are equivalent and she will answer with a word $w$ such that $\lsem M \rsem (w) \neq \lsem H \rsem (w)$ otherwise.
\end{itemize}


\subsection{Learning product automata with an \LStar{} extension}

We can use the general framework for automata learning as set up by \cite{DBLP:conf/csl/HeerdtS017}.
The general account does not directly give concrete algorithms, but it does give generalised definitions for \Def{closedness} and \Def{consistency}.
The main data structure for the algorithm is an observation table.

\begin{definition}
	An \Def{observation table} is a triple $(S, E, T)$ where $S, E \subseteq I^\ast$ are finite sets of words and $T \colon S \cup SI \to O^E$ is defined by $T(s)(e) = \lsem M \rsem (se)$.
\end{definition}

During the \LStar{} algorithm the sets $S,E$ grow and $T$ encodes the knowledge of $\lsem M \rsem$ so far.

\begin{definition}
	Let $(S,E,T)$ be an observation table.
	\begin{itemize}
		\item
		The table is \Def{product-closed} if for all $t \in SI$ there exist $s_1, s_2 \in S$ such that\break ${\pi_i T(t) = \pi_i T(s_i)}$ for $i=1,2$.
		\item
		The table is \Def{product-consistent} if for $i=1,2$ and for all $s, s' \in S$ we have\break ${\pi_i T(s) = \pi_i T(s')}$ implies ${\pi_i T(sa) = \pi_i T(s'a)}$ for all $a \in I$.
	\end{itemize}
\end{definition}

These definitions are related to the classical definitions of closedness and consistency as shown in the following lemma.
The converses of the first two points do not necessarily hold.
We also proof that if a observation table is product-closed and product-consistent, then a well-defined product machine can be constructed which is consistent with the table.

% NOTE: Without the pagebreak, the list appears on the next page, which is confusing
% as there is an algorithm in between. With the break, the lemma as a whole appears on
% the next page, without interruptions of floating figures.
\pagebreak
\begin{lemma}
	\label{lemma:implications}
	Let $OT = (S,E,T)$ be an observation table and let $\pi_i OT = (S,E,\pi_i T)$ be a component.
	The following implications hold.
	\begin{enumerate}[noitemsep]
		\item $OT$ is closed $\implies$ $OT$ is product-closed.
		\item $OT$ is consistent $\impliedby$ $OT$ is product-consistent.
		\item $OT$ is product-closed $\iff$ $\pi_i OT$ is closed for each $i$.
		\item $OT$ is product-consistent $\iff$ $\pi_i OT$ is consistent for each $i$.
	\end{enumerate}
\end{lemma}

\begin{proof}
	(1) If $OT$ is closed, then each $t \in SI$ has a $s \in S$ such that $T(t) = T(s)$.
	This implies in particular that $\pi_i T(t) = \pi_i T(s)$, as required.
	(In terms of the definition, this means we can take $s_1 = s_2 = s$.)
	
	(2) Let $OT$ be product-consistent and $s, s' \in S$ such that $T(s) = T(s')$.
	We then know that $\pi_i T(s) = \pi_i T(s')$ for each $i$ and hence $\pi_i T(sa) = \pi_i T(s'a)$ for each $i$ and $a$.
	This means that $T(sa) = T(s'a)$ as required.
		
	Statements (3) and (4) just rephrase the definitions.
\end{proof}

\begin{lemma}
	\label{lemma:construct}
	Given a product-closed and -consistent table we can define a product Moore machine consistent with the table, where each component is minimal.
\end{lemma}

\begin{proof}
	If the table $OT$ is product-closed and -consistent, then by the previous lemma, the tables $\pi_i OT$ are closed and consistent in the usual way.
	For these tables we can use the construction of \citet{DBLP:journals/iandc/Angluin87}.
	As a result we get a minimal machine $H_i$ which is consistent with table $\pi_i OT$.
	Taking the product of these gives a machine which is consistent with $OT$.
	(Beware that this product is not necessarily the minimal machine consistent with $OT$.)
\end{proof}

\begin{algorithm}[t]
	\begin{algorithmic}[1]
		\STATE Initialise $S$ and $E$ to $\{ \epsilon \}$
		\STATE Initialise $T$ with MQs
		\REPEAT
		\WHILE{$(S,E,T)$ is not product-closed or -consistent}
		\IF{$(S,E,T)$ not product-closed}
		\STATE find $t \in SI$ such that there is no $s \in S$ with $\pi_i T(t) = \pi_i T(s)$ for some $i$
		\STATE add $t$ to $S$ and fill the new row using MQs
		\ENDIF
		\IF{$(S,E,T)$ not product-consistent}
		\STATE find $s, s' \in S$, $a \in I$ and $e \in E$ such that $\pi_i T(s) = \pi_i T(s')$ but $\pi_i T(sa)(e) \neq \pi_i T(s'a)(e)$ for some $i$
		\STATE add $ae$ to $E$ and fill the new column using MQs
		\ENDIF
		\ENDWHILE
		\STATE Construct $H$ (by Lemma~\ref{lemma:construct})
		\IF{$EQ(H)$ gives a counterexample $w$}
		\STATE add $w$ and all its prefixes to $S$
		\STATE fill the new rows with MQs
		\ENDIF
		\UNTIL{$EQ(H) =$ \texttt{YES}}
		\RETURN $H$
	\end{algorithmic}
	\caption{The product-\LStar{} algorithm.}
	\label{alg:product-lstar}
\end{algorithm}

The product-\LStar{} algorithm (Algorithm~\ref{alg:product-lstar}) resembles the original \LStar{} algorithm, but uses the new notions of closed and consistent.
Its termination follows from the fact that \LStar{} terminates on both components.

By Lemma~\ref{lemma:implications} (1) we note that the algorithm does not need more rows than we would need by running \LStar{} on $M$.
By point (4) of the same lemma, we find that it does not need more columns than \LStar{} would need on each component combined.
This means that in the worst case, the table is twice as big as the original \LStar{} would do.
However, in good cases (such as the running example), the table is much smaller, as the number of rows is less for each component and the columns needed for each component may be similar.


\subsection{Learning product automata via a reduction}

The previous algorithm constructs two machines from a single table.
This suggests that we can also run two learning algorithms to construct two machines.
We lose the fact that the data structure is shared between the learners, but we gain that we can use more efficient algorithms than \LStar{} without any effort.

\begin{algorithm}[t]
	\begin{algorithmic}[1]
		\STATE Initialise two learners $L_1$ and $L_2$
		\REPEAT
		\WHILE{$L_i$ queries $MQ(w)$}
		\STATE forward $MQ(w)$ to the teacher and get output $o$
		\STATE return $\pi_i o$ to $L_i$ 
		\ENDWHILE
		\COMMENT{at this point both learners constructed a hypothesis}
		\STATE Let $H_i$ be the hypothesis of $L_i$
		\STATE Construct $H = H_1 \times H_2$
		\IF{$EQ(H)$ returns a counterexample $w$}
		\IF{$\lsem H_1 \rsem (w) \neq \pi_1 \lsem M \rsem (w)$}
		\STATE return $w$ to $L_1$
		\ENDIF
		\IF{$\lsem H_2 \rsem (w) \neq \pi_2 \lsem M \rsem (w)$}
		\STATE return $w$ to $L_2$
		\ENDIF
		\ENDIF
		\UNTIL{$EQ(H) =$ \texttt{YES}}
		\STATE return \texttt{YES} to both learners
		\RETURN $H$
	\end{algorithmic}
	\caption{Learning product machines with other learners.}
	\label{alg:general-product-learner}
\end{algorithm}

Algorithm~\ref{alg:general-product-learner} is the algorithm for learning product automata via this reduction.
It runs two learning algorithms at the same time.
All membership queries are passed directly to the teacher and only the relevant output is passed back to the learner.
(In the implementation, the query is cached, so that if the other learner poses the same query, it can be immediately answered.)
If both learners are done posing membership queries, they will pose an equivalence query at which point the algorithm constructs the product automaton.
If the equivalence query returns a counterexample, the algorithm forwards it to the learners.

The crucial observation is that a counterexample is necessarily a counterexample for at least one of the two learners.
(If at a certain stage only one learner makes an error, we keep the other learner suspended, as we may obtain a counterexample for that one later on.)
This observation means that at least one of the learners makes progress and will eventually converge.
Hence, the whole algorithm will converge.

In the worst case, twice as many queries will be posed, compared to learning the whole machine at once.
(This is because learning the full machine also learns its components.)
In good cases, such as the running example, it requires much less queries.
Typical learning algorithms require roughly $\bigO(n^2)$ membership queries, where $n$ is the number of states in the minimal machine.
For the example $M_n$ this bound gives $\bigO((n \cdot 2^n)^2) = \bigO(n^2 \cdot 2^{2n})$ queries.
When learning the components $M_n^l$ with the above algorithm, the bound gives just $\bigO((2n)^2 + \dots + (2n)^2) = \bigO(n^3)$ queries.


\section{Experiments}

We have implemented the algorithm via reduction in LearnLib.%
\footnote{The implementation and models can be found on-line at
\url{https://gitlab.science.ru.nl/moerman/learning-product-automata}.}
As we expect the reduction algorithm to be the most efficient and simpler, we leave an implementation of the direct extension of \LStar{} as future work.
The implementation handles products of any size (as opposed to only products of two machines).
Additionally, the implementation also works on Mealy machines and this is used for some of the benchmarks.

In this section, we compare the product learner with a regular learning algorithm.
We use the \TTT{} algorithm by \cite{DBLP:conf/rv/IsbernerHS14} for the comparison and also as the learners used in Algorithm~\ref{alg:general-product-learner}.
We measure the number of membership and equivalence queries.
The results can be found in Table~\ref{tab:results}.

The equivalence queries are implemented by random sampling so as to imitate the intended application of learning black-box systems.
This way, an exact learning algorithm turns into a PAC (probably approximately correct) algorithm.
Efficiency is typically measured by the total number of input actions which also accounts for the length of the membership queries (including the resets).
This is a natural measure in the context of learning black box systems, as each action requires some amount of time to perform.

We evaluated the product learning algorithm on the following two classes of machines.

\paragraph{$n$-bit register machine}
The machines $M_n$ are as described before.
We note that the product learner is much more efficient, as expected.

\paragraph{Circuits}
\begin{figure}
	\centering
	\includegraphics[width=0.28\textwidth]{img/bbara}%
	\qquad\qquad\includegraphics[width=0.28\textwidth]{img/bbaraM2}%
	\qquad\includegraphics[width=0.28\textwidth]{img/bbaraM1}
	\caption{The \texttt{bbara} circuit (left) has two output bits. This can be decomposed into two smaller circuits with a single output bit (middle and right).}
	\label{fig:circuit}
\end{figure}
In addition to the (somewhat artificial) examples $M_n$, we use circuits which appeared in the logic synthesis workshops (LGSynth89/91/93), part of the ACM/SIGDA benchmarks.%
\footnote{The original files describing these circuits can be found at
\url{https://people.engr.ncsu.edu/brglez/CBL/benchmarks/}.}
These models have been used as benchmarks before for FSM-based testing methods by \citet{DBLP:journals/cj/HieronsT15} and describe the behaviour of real-world circuits.
The circuits have bit vectors as outputs, and can hence be naturally be decomposed by taking each bit individually.
As an example, Figure~\ref{fig:circuit} depicts one of the circuits (\texttt{bbara}).
The behaviour of this particular circuit can be modelled with seven states, but when restricting to each individual output bit, we obtain two machines of just four states.
For the circuits \texttt{bbsse} and \texttt{mark1}, we additionally regrouped bit together in order to see how the performance changes when we decompose differently.

For some circuits the number of membership queries is reduced compared to a regular learner.
Unfortunately, the results are not as impressive as for the $n$-bit register machine.
An interesting case is \texttt{ex3} where the number of queries is slightly increased, but the total amount of actions performed is substantially reduced.
The number of actions needed in total is actually reduced in all cases, except for \texttt{bbsse}.
This exception can be explained by the fact that the biggest component of \texttt{bbsse} still has $25$ states, which is close to the original $31$ states.
We also note that the choice of decomposition matters, for both \texttt{mark1} and \texttt{bbsse} it was beneficial to regroup components.

In Figure~\ref{fig:stategrowth}, we look at the size of each hypothesis generated during the learning process.
We note that, although each component grows monotonically, the number of reachable states in the product does not grow monotonically.
In this particular instance where we learn \texttt{mark1} there was a hypothesis of $58\,128$ states, much bigger than the target machine of $202$ states.
This is not an issue, as the teacher will allow it and answer the query regardless.
Even in the PAC model with membership queries, this poses no problem as we can still efficiently determine membership.
However, in some applications the equivalence queries are implemented with a model checker \citep[e.g., in the work by][]{DBLP:conf/cav/Fiterau-Brostean16} or a sophisticated test generation tool.
In these cases, the increased size of intermediate hypotheses may be undesirable.

\begin{table*}
	\centering
	\begin{tabular}{l|r r|r r r|r r r|}
		& & & \multicolumn{3}{c}{Product learner} & \multicolumn{3}{c}{\TTT{} learner} \\
		Machine & States & Components & EQs & MQs & Actions & EQs & MQs & Actions \\ 
		\hline
		$M_2$ & $8$    & $2$ & $3$  & $100$    & $621$     & $5$   & $115$     & $869$      \\
		$M_3$ & $24$   & $3$ & $3$  & $252$    & $1\,855$  & $5$   & $347$     & $2946$     \\
		$M_4$ & $64$   & $4$ & $8$  & $456$    & $3\,025$  & $6$   & $1\,058$  & $13\,824$  \\
		$M_5$ & $160$  & $5$ & $6$  & $869$    & $7\,665$  & $17$  & $2\,723$  & $34\,657$  \\
		$M_6$ & $384$  & $6$ & $11$ & $1\,383$ & $12\,870$ & $25$  & $6\,250$  & $90\,370$  \\
		$M_7$ & $896$  & $7$ & $11$ & $2\,087$ & $24\,156$ & $52$  & $14\,627$ & $226\,114$ \\
		$M_8$ & $2048$ & $8$ & $13$ & $3\,289$ & $41\,732$ & $160$ & $34\,024$ & $651\,678$ \\
		\hline
		\texttt{bbara} & $7$   & $2$ & $3$  & $167$     & $1\,049$   & $3$  & $216$     & $1\,535$   \\
		\texttt{keyb}  & $41$  & $2$ & $25$ & $12\,464$ & $153\,809$ & $24$ & $6024$    & $265\,805$ \\
		\texttt{ex3}   & $28$  & $2$ & $24$ & $1\,133$  & $9\,042$   & $18$ & $878$     & $91\,494$ \\
		\texttt{bbsse} & $31$  & $7$ & $20$ & $14\,239$ & $111\,791$ & $8$ & $4\,872$ & $35\,469$ \\
		\texttt{mark1} & $202$ & $16$ & $30$ & $16\,712$ & $145\,656$ & $67$ & $15\,192$ & $252\,874$ \\
		\hline
		\texttt{bbsse}* & $31$  & $4$ & $19$ & $11\,648$ & $89\,935$ & $8$ & $4\,872$ & $35\,469$ \\
		\texttt{mark1}* & $202$ & $8$ & $22$ & $13\,027$ & $117\,735$ & $67$ & $15\,192$ & $252\,874$ \\
	\end{tabular}
	\caption{Comparison of the product learner with an ordinary learner.}
	\label{tab:results}
\end{table*}

\begin{figure}
	\centering
	\begin{tikzpicture}
	\begin{axis}[
	height=5cm,
	width=0.9\columnwidth,
	xmin=0.1,
	xmax=22.9,
	xlabel={Hypothesis-number},
	ylabel={number of states},
	ymode=log
	]
	
	\addplot[black, mark=+] plot coordinates {
		(1,   1)
		(2,   8)
		(3,   7)
		(4,   25257)
		(5,   1033)
		(6,   2312)
		(7,   855)
		(8,   896)
		(9,   615)
		(10,  865)
		(11,  55230)
		(12,  7023)
		(13,  16235)
		(14,  58128)
		(15,  1629)
		(16,  2782)
		(17,  941)
		(18,  220)
		(19,  200)
		(20,  204)
		(21,  204)
		(22,  202)
	};
	\end{axis}
	\end{tikzpicture}
	\caption{The number of states for each hypothesis while learning \texttt{mark1}.}
	\label{fig:stategrowth}
\end{figure}


\section{Discussion}
\label{sec:discussion}

We have shown two query learning algorithms which exploit a decomposable output.
If the output can be split, then also the machine itself can be decomposed in components.
As the preliminary experiments show, this can be a very effective optimization for learning black box reactive systems.
It should be stressed that the improvement of the optimization depends on the independence of the components.
For example, the $n$-bit register machine has nearly independent components and the reduction in the number of queries is big.
The more realistic circuits did not show such drastic improvements in terms of queries.
When taking the length of the queries in account as well (i.e., counting all actions performan on the system), we see an improvement for most of the test cases.

In the remainder of this section we discuss related ideas and future work.


\subsection{Measuring independence}

As the results show, the proposed technique is often beneficial but not always.
It would be interesting to know when to use decomposition.
It is an interesting question how to (quantitatively) measure the independence.
Such a measure can potentially be used by the learning algorithm to determine whether to decompose or not.


\subsection{Generalisation to subsets of products}

In some cases, we might know even more about our output alphabet.
The output set $O$ may be a proper subset of $O_1 \times O_2$, indicating that some outputs can only occur ``synchronised''.
For example, we might have $O=\{ (0,0) \} \cup \{ (a,b) \mid a,b \in [3] \}$, that is, the output $0$ for either component can only occur if the other component is also $0$.

In such cases we can use the above algorithm still, but we may insist that the teacher only accepts machines with output in $O$ for the equivalence queries (as opposed to outputs in $\{0,1,2,3\}^2$).
When constructing $H = H_1 \times H_2$ in line 7 of Algorithm~\ref{alg:general-product-learner}, we can do a reachability analysis on $H$ to check for non-allowed outputs.
If such traces exist, we know it is a counterexample for at least one of the two learners.
With such traces we can fix the defect ourselves, without having to rely on the teacher.


\subsection{Product DFAs}

For two DFAs $(Q_1, \delta_1, F_1, {q_0}_1)$ and $(Q_2, \delta_2, F_2, {q_0}_2)$, a state in the product automaton is accepting if both components are accepting.
In the formalism of Moore machines, the finals states are determined by their characteristic function and this means that the output is given by $o(q_1, q_2) = o_1(q_1) \land o_2(q_2)$.
Again, the components may be much smaller than the product and this motivated \citet{HR2013} to learn (a subclass of) product DFAs.
This type of product is more difficult to learn as the two components are not directly observable.
Such automata are also relevant in model checking and some of the (open) problems are discussed by \citet{DBLP:journals/iandc/KupfermanM15}.


\subsection{Learning automata in reverse}

The main result of \cite{DBLP:journals/jacm/RivestS94} was to exploit the structure of the so-called ``diversity-based'' automaton.
This automaton may also be called the reversed Moore machine.
Reversing provides a duality between reachability and equivalence.
This duality is theoretically explored by \citet{DBLP:journals/entcs/Rot16} and \citet{DBLP:journals/tocl/BonchiBHPRS14}
in the context of Brzozowski's minimization algorithm.

Let $M^R$ denote the reverse of $M$, then we have $\lsem M^R \rsem (w) = \lsem M \rsem (w^R)$.
This allows us to give an \LStar{} algorithm which learns $M^R$ by posing membership queries with the words reversed.
We computed $M^R$ for the circuit models and all but one of them was much larger than the original.
This suggests that it might not be useful as an optimisation in learning hardware or software systems.
However, a more thorough investigation is desired.


\subsection{Other types of composition}

The case of learning a sequential composition is investigated by \citet{DBLP:conf/nfm/0002R16}.
In their work, there are two Mealy machines, $A$ and $B$, and the output of $A$ is fed into $B$, see Figure~\ref{fig:sequential-composition}.
The goal is to learn a machine for $B$, assuming that $A$ is known (i.e., white box).
The oracle only answers queries for the sequential composition, which is defined formally as $\lsem A ; B \rsem (w) = \lsem B \rsem (\lsem A \rsem (w))$.
Since $B$ can only be interacted with through $A$, we cannot use \LStar{} directly.
The authors show how to learn $B$ using a combination of \LStar{} and SAT solvers.
Moreover, they give evidence that this is more efficient than learning $A ; B$ as a whole.

\begin{figure}
	\centering
	\begin{tikzpicture}[minimum size=3em, node distance=6em,shorten >=1pt]
	\node (L) {};
	\node[draw] (A) [right of=L] {$A$};
	\node[draw] (B) [right of=A] {$B$};
	\node (R) [right of=B] {};
	\node[draw,dashed,fit=(A) (B),inner sep=1em] (X) {};
	\node[above=-1em of X]{$A ; B$};
	
	\path[->]
	(L) edge node [above left=-10pt] {$i$} (A)
	(A) edge (B)
	(B) edge node [above right=-10pt] {$o$} (R);
	\end{tikzpicture}
	\caption{The sequential compostion $A ; B$ of two Mealy machines $A$ and $B$.}
	\label{fig:sequential-composition}
\end{figure}

An interesting generalisation of the above is to consider $A$ as an unknown as well.
The goal is to learn $A$ and $B$ simultaneously, while observing the outputs of $B$ and the communication between the components.
The authors conjecture that this would indeed be possible and result in a learning algorithm which is more efficient than learning $A ; B$ (private communication).

Another type of composition is used by \citet{DBLP:journals/tse/BolligKKL10}.
Here, several automata are put in parallel and communicate with each other.
The goal is not to learn a black box system, but to use learning when designing such a system.
Instead of words, the teacher (i.e., designer in this case) receives message sequence charts which encode the processes and actions.
Furthermore, they exploit partial order reduction in the learning algorithm.

We believe that a combination of our and the above compositional techniques can improve the scalability of learning black box systems.
Especially in the domain of software and hardware we expect such techniques to be important, since the systems themselves are often designed in a modular way. 


\acks{
We would like to thank Nathana\"{e}l Fijalkow, Ramon Janssen, Gerco van Heerdt, Harco Kuppens, Alexis Linard, Alexandra Silva, Rick Smetsers, and Frits Vaandrager for proofreading this paper and providing useful feedback.
Thanks to Andreas Abel for discussing the case of learning a sequential composition of two black box systems.
Also thanks to anonymous reviewers for interesting references and comments.}

\bibliography{References}

\end{document}
